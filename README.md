# Siglas
* API - Application Programming Interface
* ORM - Object Relational Mapper
* SGBD - Sistema de Gerenciamento de Banco de Dados
* MVC - Model View Controller


# Arquitetura de Software
## Introdução
Este documento objetiva apresentar e detalhar a arquitetura de software que será utilizada no projeto *Dessalinizador*. Dessa maneira, visa-se atingir um entendimento mútuo entre os desenvolvedores, de forma que seja padronizada a estrutura utilizada para construir o software, além das consequências que a escolha de tal arquitetura implica.

## Representação da Arquitetura
A arquitetura aqui exposta abrange dois diferentes ambientes a serem desenvolvidos: o ambiente de controle de dados e negócios, denominado Back-End (ou API), e o ambiente para interação com o usuário, caracterizado como Front-End da aplicação.

Esta separação permite uma melhor coesão de código, separando as regras de negócio e manejo de dados da interface com usuário e apresentação de informações. Além disso, esta divisão possibilita desenvolver o que se denomina Progressive Web Apps (PWAs). Este tipo de solução permite que uma aplicação seja acessível via computadores pessoais e *smartphones* (com uma experiência semelhante à apps nativos).

Uma representação macro desta arquitetura pode ser observada na Figura 1.

> Figura 1
![Arquitetura Macro](images/arquitetura-macro.png)

Os componentes de software a serem desenvolvidos, bem como as justificativas para escolhe-los estão explicitados a seguir.


### *Application Programming Interface* (Back-End)
A API será responsável por armazenar e prover todos dados da aplicação. Os dessalinizadores enviarão os dados das propriedades da água (na entrada e saída) para a API, de forma a manter uma base de dados sobre a qualidade da água numa determinada região.

Além disso, o Back-End fornecerá os dados necessários para que o Front-End transforme-os de forma a agrupá-los e gerar informações, gráficos e estatísticas.

O *framework* Rails é baseado no padrão no padrão MVC (Model View Controller). Estas três camadas se relacionam da seguinte maneira:

* **Model:** A partir de informações abstraídas do mundo real, a model é responsável pela representação e validação dos dados.

* **View:** Interface responsável pela interação com o usuário, renderizando páginas, gráficos, informações, entre outros. Não possui o processamento lógico de dados.

* **Controller:** Realiza operações ou manipulações nos dados/informações interagindo com a Model e interpretando os eventos realizados na View. Dessa maneira, essa camada é responsável por fazer a integração entre o usuário e o sistema.

Entretanto, tratando-se do modo API do *framework*, a camada *View* torna-se desnecessária, portanto não existe. Desse modo, esta parte será substituída pelo PWA em Vue.js, e as interações no diagrama permanecem as mesmas.

A figura a seguir exemplifica estas interações.

> Figura 2
![](images/rails.png)


* **falar db distribuído**
* **Diagrama classe**
* **Diagrama pacotes**


#### Justificativa
Para a elaboração do Back-End, optou-se pela utilização do *framework* Ruby on Rails. Para chegar à esta escolha a equipe levou em consideração os seguintes argumentos:

##### Maturidade
Rails foi lançado há mais de 14 anos sendo muito popular para o desenvolvimento de aplicações web, demonstrando, assim, a grande relevância e maturidade do projeto.

Esta maturidade implica numa documentação extensa e organizada, bem como uma grande comunidade de desenvolvedores. Isto leva à outro ponto que é abundância de tutoriais e soluções disponíveis em fóruns de discussão online.

O *framework* também é baseado na filosofia "Convenção sobre Configuração", de forma que, se seguidos, os padrões oferecidos pelo framework aceleram o desenvolvimento.

##### ORM
*Object Relational Mapper* (ORM) é uma técnica de programação na qual utiliza-se metadata de forma a conectar-se uma base de dados relacional à um código orientado à objetos.

A utilização dessa técnica permite escrever interfaces com o banco de dados sem utilizar a *Structured Query Language* (SQL) explicitamente. 

No *framework* Rails o componente responsável por isto chama-se *Active Record*. Desta forma as *models* são automaticamente conectadas à tabelas no banco de dados e as consultas são feitas via métodos providos pelo módulo. Isto permite que o desenvolvedor seja mais produtivo dado que não é necessário se preocupar com idiossincrasias inerentes do SGBD utilizado. Além disso, há a existência das migrações, que são *scripts* responsáveis por descrever como o banco de dados deve ser construído. Estre processo é realizado de forma automática.

Tudo isto permite que o programador foque no desenvolvimento da solução em si e não em detalhes técnicos de bancos de dados. Ademais, o código produzido torna-se mais limpo, manutenível e expressivo. 

##### *Open Source*
Rails é uma ferramenta de desenvolvimento *open source* disponibilizada gratuitamente, bem como a maior parte de suas bibliotecas. Isto implica numa gama de ferramentas oferecidas sem custos de licenças, além da existência de uma extensa comunidade de desenvolvedores dispostos a ajudar e tirar dúvidas.

##### Ferramentas
Há diversas bibliotecas, denominadas *gems*, disponíveis no ecossistema Rails. Estas bibliotecas oferecem diversas funcionalidades e facilidades, como implementação de login, níveis de autorização, facilitar a realização de testes e *debug*, dentre muitos outros recursos.

---

### *Progressive Web App (Front-End)*
O Front-End da aplicação é responsável por reunir os dados providos pelos dessalinizadores, interpreta-los e transforma-los em informações úteis. Assim é possível construir um mapa da qualidade da água numa determinada região (seja ela cidade, estado ou mesmo do Brasil). Além disso pode-se gerar gráficos sobre a qualidade da água de dessalinizadores específicos. A solução adotada para este objetivo foi a tecnologia denominada Pr*ogressive Web App*.

A ferramenta escolhida para construir esta solução foi o *framework* em Javascript Vue.js. Este utensílio utiliza o padrão MVVM, explicitado a seguir.

* **Model:** Camada responsável por guardar dados de forma a torná-los reativos. Estes dados, por vezes, são provenientes da API.

* **ViewModel:** Esta camada liga a *Model* e a *View* através de um *Binder*, de forma a conectar ações na *View* em propriedades na *Model*.

* **View:** Assim como no padrão MVC, a *View* representa a interface de comunicação com os usuários, recebendo suas interações e exibindo respostas visuais.

A Figura 3 representa esta arquitetura.
> Figura 3
![](images/mvvm.png)


#### Justificativa
Para a elaboração do Front-End optou-se pela utilização do *framework* Vue.js. Para chegar à esta escolha a equipe levou em consideração os seguintes argumentos:

##### Suporte à PWA
Os Progressives Web Apps são resultado de uma nova filosofia de desenvolvimento que busca, à partir de um mesmo código, oferecer uma aplicação web para computadores e dispositivos móveis (com uma experiência semelhante à nativa). Isto permite que a aplicação seja acessível pela maioria dos meios disponíveis atualmente.

##### Arquitetura baseada em Componentes
O *framework* Vue.js incentiva e facilita a construção de interfaces gráficas utilizando uma arquitetura de componentes. Este tipo de abordagem permite a construção de porções de códigos mais coesas.

Isto permite que o *software* desenvolvido mantenha responsabilidades separadas, independência de componentes e reutilização de código.

##### Ecossistema rico
Há diversas ferramentas disponíveis no ecossistema do Vue.js. Soluções para manter rotas (VueRouter), padrões arquiteturais para gerência de estado (Vuex), realizar requisições HTTP (Axios), dentre muitas outras.

Além disso, por se tratar de um *framework* construído utilizando a linguagem *Javascript*, têm-se à disposição diversas bibliotecas de Javascript. Isto é importante pois pode-se lançar mão das poderosas bibliotecas de gráficos existentes.
